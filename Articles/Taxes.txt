Stevens President earned over $1.6 million in 2016, Tax Documents Show

Stevens President Nariman Farvardin earned a total of $1,608,992 in compensation from the school during fiscal year 2016, according to tax documents submitted to the IRS and obtained by the Stute. This amount includes $734,013 in base pay as well as several other forms of compensation, such as an $827,800 bonus.

It is unknown at this time whether or not President Farvardin continued to earn as much in 2017.

The tax documents also shed light on how Stevens makes and spends the bulk of its funds. The school's expenses totaled $291,405,699 in the fiscal year, which spanned from July 2016 to June 2017. Of that total, $132,817,213 was spent on salaries and other employee benefits, making President Farvardin's compensation equal to 1.21% of all salaries paid by Stevens.

Other top earners at the school include then-Provost George Korfiatis, who was paid a total of $604,435, and Vice President of Finance Luis Mayer, who earned $424,546 throughout the year.

Of all Stevens's expenses, salaries and other employee compensation represented the single largest. Scholarships and grants totaled only $72,218,961 during the fiscal year, or 24.8% of all expenses.

This amount is smaller than the next-largest line item, "Other Expenses," which represented a total cost of $85,698,428 and includes a wide variety of expenses. Some of these expenses have obscure intents according to the documents viewed by the Stute. For example, $19,045,317 was spent on "Alternative Investments" with no supplementary explanation. Also, $27,618,136 was spent on investments in Central America and/or the Caribbean, the only such geographic investment present in Stevens's tax return.

The documents also reveal much information about how Stevens makes its money. A total of $332,039,245 worth of revenue was reported for the fiscal year -- leading to a net surplus of $40,633,546 at the end of the year. $240,340,542, or 72.4% of this revenue derives from tuition and fees paid by students. $20,928,590 was received from student housing costs, and $505,040 from "Technical Leadership Instruction."

$7,347,551 was received from student dining fees, while only $7,122,682 was paid to Compass Group USA, Inc, who handles food services for the school.

Stevens also received a total of $22,970,900 in gifts and contributions. Notably, this included the donation of a model ship evaluated at $4,500. The Stute's efforts to locate this ship over the course of the past two weeks resulted in failure.

Strangely, the documents show that Stevens lost a total of $235,312 on fundraising events, after making only $71,890 from those events.

Overall, Stevens students may be surprised at how much of their tution is going towards a handful of expenses that seemingly have only tangential relation to their education. The documents reviewed by the Stute are publically available for inspection here.
